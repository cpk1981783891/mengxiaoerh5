import { 
	SET_BUTTON_TEXT,
	SET_USER
} from '../mutation-types/global.js'

export default {
	[SET_BUTTON_TEXT]: (state, text) => {
		state.buttonText = text
	},
	[SET_USER]: (state,text) => {
		state.user = text
	},
	//判断当前平台
	setSource: (state) => {
		const u = navigator.userAgent;
		const isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
		if (isiOS) {
			state.source = "ios";
		} else {
			state.source = "andriod";
		}
	}
}
