export default {
	setStep2val: (state, text) => {
		state.uid = text.uid; //商户UID
		state.suid = text.suid;//门店ID
	},
	setLocation: (state, text) => {
		state.location = text;
	},
	//清空商户认证数据
	clearval: (state) => {
		state.isfoodChain = '';
		state.uid = '';
		state.suid = '';
		state.code_id = '';
		state.location = '';
	}
}
