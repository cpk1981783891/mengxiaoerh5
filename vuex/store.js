import Vue from 'vue'
import Vuex from 'vuex'
import global from './modules/global.js'
import merchantver from './modules/merchantver.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		global,
		merchantver //商户认证
	}
})

export default store
