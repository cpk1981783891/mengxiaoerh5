import { 
	SET_BUTTON_TEXT,
	SET_USER
} from '../mutation-types/global.js'
import { user } from '../../api/index.js'
import storage from '../../util/storage.js'

export default {
	setButtonText({commit}, text) {
		commit(SET_BUTTON_TEXT, text)
	},
	//退出登录
	exitLogin(){
		cm.jumpFunction({
			method:'push',
			url:'/login',
			data:{},
			success:function () {
				cm.storagFunction({
					method:'removeAllItemSavekey',
					indexName:['phoneList'],
					success:function () {}
				});
			}
		});
		
		// cm.storagFunction({
		// 	method:'getItem',
		// 	indexName:'phoneList',
		// 	success:function(res){
		// 		let phoneList = res;
		// 		cm.jumpFunction({
		// 			method:'push',
		// 			url:'/login',
		// 			data:{},
		// 			success:function () {
		// 				cm.storagFunction({
		// 					method:'removeAllItem',
		// 					success:function () {
		// 						cm.storagFunction({
		// 							method:'setItem',
		// 							indexName:'phoneList',
		// 							data:phoneList
		// 						});
		// 					}
		// 				});
		// 			}
		// 		});
		// 	}
		// });
	},
	// 设置(更新)用户信息
	setUser({commit},text){
		const id = storage.getRepository('global').getItemSync('loginInfo').aid;
		user({id:id}).then((res)=>{
			commit(SET_USER,res.data)
		})
	}
}
