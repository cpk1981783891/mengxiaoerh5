import mutations from '../mutations/global.js'
import actions from '../actions/global.js'
import getters from '../getters/global.js'

const state = {
	buttonText: '更改 vuex 内容',
	source:'', //当前移动设备类型 ios/andriod
	user:''  //当前用户信息
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
