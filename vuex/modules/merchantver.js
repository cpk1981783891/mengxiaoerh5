import mutations from '../mutations/merchantver.js'
import getters from '../getters/merchantver.js'
const state = {
	isfoodChain:'', //当前标签是否是餐饮
	uid:'', //商户UID
	suid:'',//门店ID
	code_id:'',//收银点ID
	location:'',
	banks:'', //所属银行数据
	categorys:'' //经营品类数据
}

export default {
	namespaced: true,
	state,
	mutations,
	getters
}
