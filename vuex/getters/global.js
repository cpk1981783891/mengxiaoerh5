export default {
	buttonText: (state)=> {
		return state.buttonText
	},
	user: (state)=> {
		return state.user
	},
	source: (state)=> {
		return state.source
	}
}
