var cm = (function() {
  var cm = {}
  var eventList = {}
  var apiEventList = {}
  var apiList = [
      'chooseImage',
      'makePhoneCall',
      'chooseLocation',
      'getLocation',
      'scanCode',
      'bluetoothGetDevices',
      'blueToothAutoConnect',
      'blueToothConnect',
      'bluetoothPrinte',
      'getContacts',
      "uploadVideoToLocal",
      "uploadVideoToTencentCloud",
	  'jumpTypeChange',
	  'jumpLogin',
	  'jumpFunction',
	  'getVersions',
	  'getLocations',
	  'jumpMap',
	  'storagFunction',
	  'chooseVideo',
	  'videoFunction',
	  'searchPOIAround',
	  'singleLocation',
	  'registerWithKey',
	  'searchPOI',
	  'bluetoothGetDevices2',
	  'bluetoothGetDevicesIOS2',
	  'blueToothAutoConnect2',
	  'blueToothConnect2',
	  'bluetoothPrinte2',
	  'UpdateInfo'
  ]

  //获取随机字符串
  function randomStr() {
      var str = Math.random().toString(32).slice(2, 12);
      if(apiEventList[str]) {
          return randomStr()
      }
      return str
  }

  //获取系统信息
  function systemInfo() {
      var u = navigator.userAgent, app = navigator.appVersion;
      var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //g
      var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

      if(isAndroid) {
          return 'android'
      }
      if(isIOS) {
          return 'ios'
      }
      return 'other'
  }

  //1.为自定义事件类型, 2.为api类型
  function emitWeexEvent(name, data, id) {
      var newId = id ? id : ''
		  // console.log(JSON.stringify({name, data, id: newId}))
		  // console.log(systemInfo())
      if(systemInfo() === 'android') {
          android.notifyWeex(JSON.stringify({name, data, id: newId}))
      }else if(systemInfo() === 'ios') {
			window.webkit.messageHandlers.callNative.postMessage({
		        name, data, id: newId,
		    })
          // $notifyWeex({name, data, id: newId})
      }
  }

  //监听 weex 触发的事件
  document.addEventListener('notify', function (e) {
      var data = e.detail
      if(data.id) { //api回馈事件
          if(apiEventList[data.id] && data.isSuccess == 1) {
              apiEventList[data.id].success && apiEventList[data.id].success(data.data)
          }else {
              apiEventList[data.id].fail && apiEventList[data.id].fail(e.detail.data)
          }
      }else { //自定义事件
          if(data.name && eventList[data.name]) {
              for(var i = 0; i < eventList[data.name].length; i++) {
                  eventList[data.name][i](data.data)
              }
          }
      }
  })

  //注册事件
  cm.on = function (name, callback) {
      if(typeof name !== 'string') {
          throw new Error('event name must be string')
      }
      if(typeof callback !== 'function') {
          throw new Error('event callback must be function')
      }

      if(!eventList[name]) {
          eventList[name] = []
      }
      eventList[name].push(callback)
  }

  //触发事件
  cm.emit = emitWeexEvent

  //循环注册weex api
  for(var i = 0; i < apiList.length; i++) {
      (function (i) {
          var name = apiList[i]
          cm[name] = function(op) {
              var flag = randomStr()
              apiEventList[flag] = {
                  success: op.success || '',
                  fail: op.fail || ''
              }
              op.success && delete op.success
              op.fail && delete op.fail

              //触发weex事件
              emitWeexEvent(name, op, flag)
          }
      })(i)
  }

  return cm
}())