import http from '../util/http.js'
import http2 from '../util/http2.js'
import config from '../util/config.js'

const VERSION_CODE = config.mengxiaoer.VERSION_CODE
const RETAIL = config.mengxiaoer.RETAIL //零售

//登录
export const login = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/login', data)
}

//===首页(工作台)===
//首页(工作台)顶部数据
export const index = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/index', data)
}
//首页(工作台)商户列表数据
export const shoplist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shoplist', data)
}
//首页(工作台)商户列表数据详情
export const shopdetail = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopdetail', data)
}
//更新商户的信息(名称、服务号码、定位信息)
export const updateshopinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/updateshopinfo', data)
}
//商户详情-基本信息
export const shopdetailbase = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopdetailbase', data)
}
//商户详情-设备信息
export const shopdetailorder = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopdetailorder', data)
}
//商户详情-设备信息
export const shopdetaildevice = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopdetaildevice', data)
}
//商户地址判断(判断两个地址是否超过100米)
export const outofscope = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/outofscope', data)
}
//外卖商户-商户列表
export const waimaishoplist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/waimaishoplist', data)
}
//外卖商户-商户详情
export const waimaishopdetailbase = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/waimaishopdetailbase', data)
}
//公海商户-商户列表
export const seasshop = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/seasshop', data)
}
//公海商户-商户详情
export const seasshopinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/seasshopinfo', data)
}
//外卖商户-订单数据
export const waimaishoporder = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/waimaishoporder', data)
}
//外卖商户-修改费率
export const changeshoprate = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/changeshoprate', data)
}
//平台分发商户列表（待处理门店列表）
export const newshops = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/newshops', data)
}


//==数据==
//商户-用户数据
export const datacenter = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/datacenter', data)
}

//===服务===
//零售协助报件--扫码(获取商户认证信息)
export const assistauthscan = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/assistauthscan', data)
}
//云闪付商户列表
export const ysflist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/ysflist', data)
}
//云闪付物料列表
export const ysfimglist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/ysfimglist', data)
}
//云闪付与云闪付物料详情
export const ysfinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/ysfinfo', data)
}
//云闪付物料添加接口
export const ysfimgadd = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/ysfimgadd', data)
}
//云闪付添加接口
export const ysfadd = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/ysfadd', data)
}
//设备面板(设备列表)
export const devicedash = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicedash', data)
}
//设备激活列表(设备详情列表--设备关联)
export const devicebindlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicebindlist', data)
}
//设备激活详情(设备详情--设备关联详情)
export const devicebinddetail = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicebinddetail', data)
}
//设备详情(设备详情--设备绑定-设备绑定详情)
export const devicedetail = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicedetail', data)
}
//设备激活列表(设备详情列表--设备绑定)
export const devicelist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicelist', data)
}
//设备扫码激活
export const devicebindscan = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/devicebindscan', data)
}
//商圈商户列表
export const shopmlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopmlist', data)
}
// 获取商圈
export const shopmscan = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopmscan', data)
}
// 获取商圈
export const shopmhome = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopmhome', data)
}
// 绑定商圈
export const shopmbind = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopmbind', data)
}
//运维商户添加(商户活动-添加商圈)
export const shopmadd = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopmadd', data)
}
//商户活动列表
export const shopratelist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopratelist', data)
}
//商户活动详情
export const shoprateinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shoprateinfo', data)
}
//商户数据
export const shopinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopinfo', data)
}
//商户费率申请
export const shoprateadd = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shoprateadd', data)
}
//零售协助报件--分类信息
export const getbaseinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/getbaseinfo', data)
}
//获取门店标签
export const wmshoptags = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/wmshoptags', data)
}
//零售协助报件--支行信息
export const bankarea = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/bankarea', data)
}
//零售协助报件--城市信息
export const getbankcity = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/getbankcity', data)
}
//零售协助报件--提交信息(商户认证第二步-提交)
export const assistauthinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/assistauthinfo', data)
}
//零售协助报件--绑定台卡(商户认证第二步-扫码)
export const assistauthcard = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/assistauthcard', data)
}
//零售协助报件--上传资料(商户认证第三步-提交)
export const assistauthupload = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/assistauthupload', data)
}
//零售协助报件--驳回资料(商户认证第三步-获取资料)
export const assistauthreup = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/assistauthreup', data)
}
//零售新协助报件--设置类型
export const settype = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/settype', data)
}
//外卖审核列表(我处理的-外卖)
export const verifywaimailist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/verifywaimailist', data)
}
//外卖审核详情(我处理的-外卖详情)
export const verifywaimaidetail = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/verifywaimaidetail', data)
}
//外卖审核(我处理的-外卖详情-审核)
export const verifywaimai = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/verifywaimai', data)
}
//发起审核列表(我处理的-费率)
export const auditlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/auditlist', data)
}
//商户费率审核(我处理的-费率详情-审核)
export const shoprateaudit = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shoprateaudit', data)
}
//门店信息审批单列表(我处理的-店铺)
export const wmmodifylist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/wmmodifylist', data)
}
//门店信息审批单详情(我处理的-店铺详情)
export const wmmodifyinfo = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/wmmodifyinfo', data)
}
//审核操作(我处理的-店铺详情-审核)
export const wmmodifyverify = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/wmmodifyverify', data)
}
//商品审批单列表(我处理的-菜品)
export const verifygoodslist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/verifygoodslist', data)
}
//审核商品列表(我处理的-菜品详情)
export const goodslist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/goodslist', data)
}
//审核商品(我处理的-菜品详情-审核)
export const verifygoods = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/verifygoods', data)
}
//发起审核列表
export const userauditlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userauditlist', data)
}
//获取积分包批次列表
export const batchlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/batchlist', data)
}
//积分包二维码列表
export const codepackagelist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/codepackagelist', data)
}
//打印二维码列表
export const printqrcode = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/printqrcode', data)
}
//门店码-获取门店列表
export const alipayshoplist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/alipayshoplist', data)
}
//门店码-申请门店码
export const applyshopcode = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/applyshopcode', data)
}
//门店码-上传图片并获取图片ID
export const uploadalipaypic = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/uploadalipaypic', data)
}
//门店码门店类目查询
export const alipaycategory = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/alipaycategory', data)
}
// 门店码-绑定物料
export const bindmaterials = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/bindmaterials', data)
}
//门店码-获取所有地址码
export const getcitycode = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/getcitycode', data)
}
//门店码-补充资料
export const updatem2 = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/updatem2', data)
}
//门店码-获取商户数据
export const shopinfonolimit = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/shopinfonolimit', data)
}
//门店码-再次提交
export const applyshopcoderepeat = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/applyshopcoderepeat', data)
}
//数据报表-商户数据
export const statshoplist = (data) => {
   return http.post(RETAIL+VERSION_CODE+'/statshoplist', data)
}
//数据报表-刷脸设备交易统计数据
export const statsl = (data) => {
   return http.post(RETAIL+VERSION_CODE+'/statsl', data)
}

//===我的===
//获取用户信息
export const user = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/user', data)
}
//获取可切换的身份列表
export const userswitchlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userswitchlist', data)
}
//切换身份
export const userswitch = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userswitch', data)
}
//获取业务员列表(我的下级列表)
export const userlist = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userlist', data)
}
//用户身份(获取用户等级)
export const useridentity = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/useridentity', data)
}
//添加业务员/代理商
export const useradd = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/useradd', data)
}
//获取验证码
export const sendsms = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/sendsms', data)
}
//app升级(获取当前版本)
export const version = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/version', data)
}
//用户信息修改
export const userupdate = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userupdate', data)
}
//修改密码
export const userpassword = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userpassword', data)
}

//公共
//用户二维码信息
export const userqrmsg = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/userqrmsg', data)
}
//上传图片
export const uploadimg = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/uploadimg', data)
}
//获取上传视频的key
export const uploadtempkeys = (data = {})=> {
	return http.post(RETAIL+VERSION_CODE+'/uploadtempkeys', data)
}
//图片自动识别
export const getPhotoInfo = (data = {})=> {
	return http2.post('/get_photo_info', data)
}