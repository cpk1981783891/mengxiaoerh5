import httpGet from '../util/httpGet.js'
import config from '../util/config.js'

//此文件专门处理get请求接口

//获取协助报件信息
export const getAuthinfo = (data = {})=> {
	return httpGet.get(data.url)
}