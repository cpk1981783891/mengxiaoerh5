import Log from '../lib/log.js'
import config from './config.js'

export const isIphoneX = ()=> {
	const platform = uni.getSystemInfoSync()

	// iPhone X、iPhone XS
	const isIPhoneX = /iphone/.test(platform.model.toLocaleLowerCase()) && Number(platform.screenWidth) == 375 && Number(
	  platform.screenHeight) == 812
	// iPhone XS Max
	const isIPhoneXSMax = /iphone/.test(platform.model.toLocaleLowerCase()) && Number(platform.screenWidth) == 414 &&
	  Number(platform.screenHeight) == 896
	// iPhone XR
	const isIPhoneXR = /iphone/.test(platform.model.toLocaleLowerCase()) && Number(platform.screenWidth) == 414 && Number(
	  platform.screenHeight) == 896
	if (isIPhoneX || isIPhoneXSMax || isIPhoneXR) {
	  return true
	}
	
	return false
} 

export const promisify = (func, data = {})=> {
	return new Promise((resolve, reject)=> {
		func({
			...data,
			success(result) {
				resolve(result)
			},
			fail(error) {
				reject(error)
			}
		})
	})
}


export const log = new Log({
	name: config.mengxiaoer.name,
	id: config.mengxiaoer.appId,
	version: config.mengxiaoer.version,
	report: config.mengxiaoer.report,
	userToken: async ()=> {
		//这里写获取用户信息的方法
		return ''
	}
})

