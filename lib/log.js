function getSystemInfo() {
	return new Promise((resolve, reject)=> {
		uni.getSystemInfo({
			success(res) {
				resolve({
					dev_model: res.model,
					dev_id: '',
					dev_sys_type: res.platform,
					dev_sys_version: res.system,
					// #ifdef MP
					dev_platform_p_type: 'MP', //上报程序类型 APP MP H5
					// #endif
					// #ifdef H5
					dev_platform_p_type: 'H5', //上报程序类型 APP-MP-H5-
					// #endif
					// #ifdef MP-WEIXIN
					dev_platform_type: 'MP-WEIXIN',
					dev_platform_version: res.SDKVersion,
					// #endif
					// #ifdef MP-ALIPAY
					dev_platform_type: 'MP-ALIPAY',
					dev_platform_version: res.app,
					// #endif
				})
			},
			fail(err) {
				reject(err)
			}
		})
	})
}

function getNetWork(){
	return new Promise((resolve, reject)=>{
		uni.getNetworkType({
			success(res) {
				resolve({
					net_type: res.networkType
				})
			},
			fail(err) {
				reject(err)
			}
		})
	})
}

export default class Log {
	constructor(op) {
		if(!op.name) {
			throw new Error('请传入项目名称')
		}
		if(!op.id) {
			throw new Error('请传入项目id')
		}
		if(!op.version) {
			throw new Error('请传入项目版本号')
		}
		if(!op.userToken) {
			throw new Error('请传入 userToken 方法')
		}
		this.cacheLogs = []
		this.timer = ''
		
		this.isReport = op.report
		this.userToken = op.userToken
		
		this.info = {
			app_name: op.name || '' ,
			app_id: op.id || '',
			app_desc: op.desc || '',
			app_version: op.version || ''
		}
		this.getInfo()
	}
	async getInfo() {
		const data = await getSystemInfo()
		this.info = Object.assign({}, this.info, data)
	}
	async reportRequest(res) {
		const network = await getNetWork()
		const info = Object.assign({}, this.info, network, {
			req_id: res.config.requestUUID,
			req_p_id: '',
			url: `${res.config.baseUrl}${res.config.url}`,
			method: res.config.method,
			req_header: res.config.header,
			req_body: res.config.data,
			req_start_time: res.config.requestStartTime,
			req_duration: Date.now() - res.config.requestStartTime,
			req_func: 'request',
			status_code: res.statusCode,
			res_header: res.header,
			res_body: res.data,
			res_biz_msg: res.data.msg,
			res_biz_code: res.data.code,
			custom_detail: '',
		})
		
		clearTimeout(this.timer)
		this.cacheLogs.push(info)
		this.timer = setTimeout(()=>{
			let arr = JSON.stringify(this.cacheLogs)
			this.cacheLogs = []
			this.report(JSON.parse(arr), '/add/timelog')
		}, 5000)
	}
	async reportApi(err) {
		const network = await getNetWork()
		let pages = getCurrentPages()
		pages = pages.map(item=>{
			return item.route
		})
		try{
			
			let tokenInfo = typeof this.userToken === 'function' ? await this.userToken() : this.userToken;
			const info = Object.assign({}, this.info, network, {
				page_name: pages,
				error_desc: err.message,
				error_code: err.msgCode,
				error_stack: err.stack ? err.stack : JSON.stringify(err),
				user_id: tokenInfo
			})
			this.report(info, '/add/errorlog')
		}catch(e){
			console.log(e)
		}
	}
	report(data, url) {
		if(this.isReport) {
			const base = process.env.NODE_ENV === 'production' ? 'https://report.triumen.cn' : 'http://192.168.0.2:8899'
			uni.request({
				url: `${base}${url}`,
				header: {
					'Content-Type': 'application/json'
				},
				method: 'POST',
				data: data
			})
		}
	}
}