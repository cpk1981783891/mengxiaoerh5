/**
 * @param { String } 仓库名
 */
class Repository {
  constructor (name) {
    this.ext = name
  }
  /**
   * 储存本地数据
   * @param {String} key
   * @param {Object | String} data
   */
  setItem (key, data) {
    return new Promise((resolve, reject) => {
      uni.setStorage({
        key: `${this.ext}_${key}`,
        data,
        success () {
          resolve()
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }
  /**
   * 储存本地数据同步版本
   * @param {String} key
   * @param {Object | String} data
   */
  setItemSync (key, data) {
     return uni.setStorageSync(`${this.ext}_${key}`, data)
  }
  /**
   * 获取本地储存
   * @param {string} key
   */
  getItem (key) {
    return new Promise((resolve, reject) => {
      uni.getStorage({
        key: `${this.ext}_${key}`,
        success (result) {
          resolve(result.data ? result.data : result.APDataStorage)
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }
  /**
   * 获取本地储存同步版
   * @param {string} key
   */
  getItemSync (key) {
    return uni.getStorageSync(`${this.ext}_${key}`)
  }
  /**
   * 删除缓存
   * @param {string} key
   */
  removeItem (key) {
    return new Promise((resolve, reject) => {
      uni.removeStorage({
        key: `${this.ext}_${key}`,
        success (res) {
          resolve(res)
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }
  /**
   * 删除缓存同步版
   * @param {string} key
   */
  removeItemSync (key) {
    return uni.removeStorageSync(`${this.ext}_${key}`)
  }
  /**
   * 删除本地的所有缓存
   */
  removeAllItem () {
    return new Promise((resolve, reject) => {
      uni.clearStorage()
			resolve()
    })
  }
  /**
   * 删除本地的所有缓存同步版
   */
  removeAllItemSync () {
    return uni.clearStorageSync()
  }
  /**
   * 获取本地缓存的信息
   */
  getStorageInfo () {
    return new Promise((resolve, reject) => {
      uni.getStorageInfo({
        success (res) {
          resolve(res)
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }
  /**
   * 获取本地缓存的信息同步版
   */
  getStorageInfoSync () {
    return uni.getStorageInfoSync()
  }
}

export default class Storage {
  constructor () {
    this.repositories = {}
  }
  /**
   * 创建储存仓库
   * @param {string} name
   */
  create (name) {
    if (!name) {
      throw new Error('请传入仓库名')
    }
    if (!!name && typeof name !== 'string') {
      throw new Error('仓库名应为字符串')
    }
    if (Object.keys(this.repositories).indexOf(name) >= 0) {
      throw new Error(`${name} 仓库已经存在`)
    }

    this.repositories[name] = new Repository(name)
  }
  /**
   * 获取储存仓库
   * @param {string} name
   */
  getRepository (name) {
    if (Object.keys(this.repositories).indexOf(name) < 0) {
      throw new Error(`${name}仓库不存在, 请先创建`)
    }
    return this.repositories[name]
  }
}
