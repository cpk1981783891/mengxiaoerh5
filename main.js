import Vue from 'vue'
import App from './App'
import storage from './util/storage.js'
import store from './vuex/store.js'
import uView from 'uview-ui'

Vue.config.productionTip = false

App.mpType = 'app'
 

Vue.prototype.$storage = storage.getRepository.bind(storage)

Vue.use(uView);

const app = new Vue({
    ...App,
	store
})
app.$mount()
